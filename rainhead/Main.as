package rainhead
{
	import flash.display.Sprite;
	import flash.errors.IOError;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.KeyboardEvent;
	import flash.events.NetDataEvent;
	import flash.events.ProgressEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.text.TextField;
	import flash.text.TextFieldType;
	import flash.ui.Keyboard;
	import flash.utils.getTimer;
	import net.hires.debug.Stats;
	
	/**
	 * ...
	 * @author Mc
	 */
	public class Main extends Sprite 
	{
		//Links like: "https://dl.dropboxusercontent.com/u/97263517/auctions_231.json" works from Project
		//From browser only local files can be loaded
		private const URL:String = "../assets/github.json";
		private var loader:URLLoader;
		private var parser:JSONParser;
		private var input:TextField;
		private var output:TextField;
		
		public function Main():void 
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			initLoader();	
			initParser();
			initInput();
			initOutput();
			addChild(new Stats());
			addEnterListeners();
		}
		
		private function initLoader():void 
		{
			loader = new URLLoader();
			loader.addEventListener(ProgressEvent.PROGRESS, onProgress);
			loader.addEventListener(Event.COMPLETE, onComplete);
			loader.addEventListener(IOErrorEvent.IO_ERROR, onError);
		}
		
		private function initParser():void 
		{
			parser = new JSONParser();
			parser.addEventListener(Event.COMPLETE, onParseComplete);
		}
		
		private function initInput():void 
		{
			input = new TextField();
			input.border = true;
			input.width = 200;
			input.height = 25;
			input.x = 50;
			input.y = 125;
			input.text = URL;
			addChild(input);
			input.type = TextFieldType.INPUT;
		}
		
		private function initOutput():void 
		{
			output = new TextField();
			output.border = true;
			output.width = 200;
			output.height = 25;
			output.x = 50;
			output.y = 150;
			output.text = "Output text will be displayed here.";
			addChild(output);
		}
		
		private function onKey(e:KeyboardEvent):void 
		{
			if (e.keyCode != Keyboard.ENTER) return;
			//If enter when no data loaded
			if (loader.data == null) {
				removeEnterListeners();
				loader.load(new URLRequest(input.text));
			}
			//If enter when JSON loaded through URLLoader
			else {
				removeEnterListeners();
				output.text = "Parsing started at: " + getTimer().toString() + " ms.";
				parser.parse(loader.data);
			}
		}
		
		private function onProgress(e:ProgressEvent):void 
		{
			output.text = "Loading: " + loader.bytesLoaded + " of " + loader.bytesTotal + " bytes.";
		}
		
		private function onComplete(e:Event):void 
		{
			output.text = "Loading completed: " + loader.bytesTotal + " bytes.";
			addEnterListeners();
		}
		
		private function onParseComplete(e:Event):void 
		{
			var result:Object = parser.getResult();
			trace(result);
			loader.data = null;
			output.text = "Parsing finished at: " + getTimer().toString() + " ms.";
			addEnterListeners();
		}
		
		private function onError(e:IOErrorEvent):void 
		{
			output.text = e.text;
			loader.data = null;
			addEnterListeners();
		}
		
		private function addEnterListeners():void 
		{
			input.addEventListener(KeyboardEvent.KEY_UP, onKey);
		}
		
		private function removeEnterListeners():void 
		{
			input.removeEventListener(KeyboardEvent.KEY_UP, onKey);
		}
	}
}