package rainhead 
{
	import flash.display.Shape;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.utils.getTimer;
	import rainhead.data.JSONData;
	/**
	 * ...
	 * @author Mc
	 */
	public class JSONParser extends EventDispatcher
	{	
		//Budget is calculated by taking half time of one frame
		private const ms:int = 1000;
		private const fps:int = 30;
		private const budget:Number = (ms / fps) * 0.5;
		
		//Shape is used to call EnterFrame Event
		private var enterFrame:Shape;
		private var startTime:Number;
		
		//Current JSON file
		private var jsonString:String;
		private var length:int;
		//Current position in parsing
		private var buffer:int;
		private var result:Object;
		//Stack is used to store the last container in JSON tree
		private var stack:Array;
		
		public function JSONParser() 
		{
			enterFrame = new Shape();
		}
		
		public function parse(jsonString:String):void 
		{
			this.jsonString = jsonString;
			length = jsonString.length;
			buffer = 0;
			initStack();
			enterFrame.addEventListener(Event.ENTER_FRAME, onParseEnter);
		}
		
		private function initStack():void 
		{
			stack = [];
			var initChar:String = jsonString.charAt(0);
			//If JSON starts with Object
			if (initChar == JSONData.JSON_OBJECT_OPEN) {
				result = {};
				buffer++;
			}
			//If JSON starts with Array
			else if (initChar == JSONData.JSON_ARRAY_OPEN) {
				result = [];
				buffer++;
			}
			//If JSON contains only single Value
			else {
				result = [];
			}
			stack.push(result);
		}
		
		private function onParseEnter(e:Event):void 
		{
			startTime = getTimer();
			var i:int = 0;
			while (getTimer() < startTime + budget) {
				parseBuffer();
				//Check if String is parsed
				if (buffer >= length) {
					enterFrame.removeEventListener(Event.ENTER_FRAME, onParseEnter);
					dispatchEvent(new Event(Event.COMPLETE));
					return;
				}
			}
		}
		
		private function parseBuffer():void 
		{
			//Prepare
			eatWhitespace();
			if (buffer >= length) return;
			var token:String = getToken();
			var index:int = getCurrentIndex();
			//Temp
			var o:Object;
			var a:Array;
			//Switch
			switch (token) {
				case JSONData.JSON_OBJECT_OPEN: //Object pushed to Array
					o = {};
					(stack[index] as Array).push(o);
					stack.push(o);
					break;
				case JSONData.JSON_OBJECT_CLOSE:
					stack.pop();
					break;
				case JSONData.JSON_ARRAY_OPEN: //Array pushed to Array
					a = [];
					(stack[index] as Array).push(a);
					stack.push(a);
					break;
				case JSONData.JSON_ARRAY_CLOSE:
					stack.pop();
					break;
				case JSONData.JSON_STRING: //Attribute of Object || String pushed to Array
					var attr:String = eatString();
					eatSpecial();
					eatWhitespace();
					token = getToken();
					if (token == JSONData.JSON_COLON)
					{
						buffer++;
						eatWhitespace();
						token = getToken();
						switch (token) 
						{
							case JSONData.JSON_OBJECT_OPEN: //Object as Value of Attribute
								o = {};
								(stack[index])[attr] = o;
								stack.push(o);
								break;
							case JSONData.JSON_ARRAY_OPEN: //Array as Value of Attribute
								a = [];
								(stack[index])[attr] = a;
								stack.push(a);
								break;
							case JSONData.JSON_STRING: //String as Value of Attribute
								(stack[index])[attr] = eatString();
								break;
							default:
								if (JSONData.JSON_DIGIT(token)) (stack[index])[attr] = eatDigit(); //Digit as Value of Attribute
								else if (JSONData.INIT_TRUE(token)) (stack[index])[attr] = eatTrue(); //TRUE as Value of Attribute
								else if (JSONData.INIT_FALSE(token)) (stack[index])[attr] = eatFalse(); //FALSE as Value of Attribute
								else (stack[index])[attr] = eatNull(); //Anything stored into NULL as Value of Attribute
								break;
						}
					}
					else {
						(stack[index] as Array).push(attr); //String pushed to Array
					}
					break;
				case JSONData.JSON_COMMA: //Comma separating Items
					break;
				default:
					//Single Values stored into Array
					if (JSONData.JSON_DIGIT(token)) (stack[index] as Array).push(eatDigit());
					else if (JSONData.INIT_TRUE(token)) (stack[index] as Array).push(eatTrue());
					else if (JSONData.INIT_FALSE(token)) (stack[index] as Array).push(eatFalse());
					else (stack[index] as Array).push(eatNull());
					break;
			}
			buffer++;
		}
		
		private function eatWhitespace():void 
		{
			//Skip invisible chars
			while (JSONData.JSON_WHITESPACE(jsonString.charAt(buffer))) {
				buffer++;
			}
		}
		
		private function eatSpecial():void 
		{
			//Skip quotation chars near String
			if (jsonString.charAt(buffer) == JSONData.JSON_STRING) {
				buffer++;
			}
		}
		
		private function eatString():String 
		{
			var s:String = "";
			eatSpecial();
			while (jsonString.charAt(buffer) != JSONData.JSON_STRING) {
				s += jsonString.charAt(buffer);
				buffer++;
			}
			return s;
		}
		
		private function eatDigit():Number
		{
			var s:String = "";
			while (JSONData.JSON_DIGIT(jsonString.charAt(buffer))) {
				s += jsonString.charAt(buffer);
				buffer++;
			}
			buffer--; //Revert buffer to last Digit
			return parseInt(s, 10);
		}
		
		private function eatTrue():Boolean
		{
			//If buffer exceeds size return null
			if ((buffer + 3) > length) {
				return eatNull();
			}
			//Take chars from String from [buffer, buffer + 3]
			var _true:String = jsonString.charAt(buffer) + jsonString.charAt(buffer + 1) + jsonString.charAt(buffer + 2);
			_true += jsonString.charAt(buffer + 3);
			_true = _true.toUpperCase();
			if (_true == JSONData.JSON_TRUE) {
				buffer = buffer + 3;
				return true;
			}
			else {
				return eatNull();
			}
		}
		
		private function eatFalse():Boolean 
		{
			if ((buffer + 4) > length) {
				return eatNull();
			}
			//Take chars from String from [buffer, buffer + 4]
			var _false:String = jsonString.charAt(buffer) + jsonString.charAt(buffer + 1) + jsonString.charAt(buffer + 2);
			_false +=  jsonString.charAt(buffer + 3) + jsonString.charAt(buffer + 4);
			_false = _false.toUpperCase();
			if (_false == JSONData.JSON_FALSE) {
				buffer = buffer + 4;
				return false;
			}
			else {
				return eatNull();
			}
		}
		
		private function eatNull():Object 
		{
			eatAlpha();
			return null;
		}
		
		private function eatAlpha():void 
		{
			//Skip alphabetic chars
			while (JSONData.JSON_ALPHA(jsonString.charAt(buffer))) {
				buffer++;
			}
		}
		
		private function getToken():String 
		{
			//Take char from String at current buffer
			return jsonString.charAt(buffer);
		}
		
		private function getCurrentIndex():int
		{
			return stack.length - 1;
		}
		
		public function getResult():Object
		{
			return result;
		}
	}
}