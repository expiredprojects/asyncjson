package rainhead.data 
{
	/**
	 * ...
	 * @author Mc
	 */
	public class JSONData 
	{
		//Char
		public static const JSON_OBJECT_OPEN:String = '{';
		public static const JSON_OBJECT_CLOSE:String = '}';
		public static const JSON_ARRAY_OPEN:String = '[';
		public static const JSON_ARRAY_CLOSE:String = ']';
		public static const JSON_STRING:String = '"';
		public static const JSON_COMMA:String = ',';
		public static const JSON_COLON:String = ':';
		
		//Range
		public static function JSON_DIGIT(val:String):Boolean {
			return ((val >= '0' && val <= '9') || val == '-' || val == '.');
		}
		public static function JSON_ALPHA(val:String):Boolean {
			return ((val >= 'a' && val <= 'z') || (val >= 'A' && val <= 'Z'));
		}
		public static function JSON_WHITESPACE(val:String):Boolean {
			return (val == ' ' || val == '\n' || val == '\r' || val == '\t');
		}
		
		//Strings
		public static const JSON_TRUE:String = "TRUE";
		public static const JSON_FALSE:String = "FALSE";
		public static const JSON_NULL:String = "NULL";
		
		//Inits
		public static function INIT_TRUE(val:String):Boolean {
			return (val == JSON_TRUE.charAt(0) || val == JSON_TRUE.charAt(0).toLowerCase());
		}
		public static function INIT_FALSE(val:String):Boolean {
			return (val == JSON_FALSE.charAt(0) || val == JSON_FALSE.charAt(0).toLowerCase());
		}
		public static function INIT_NULL(val:String):Boolean {
			return (val == JSON_NULL.charAt(0) || val == JSON_NULL.charAt(0).toLowerCase());
		}
	}
}